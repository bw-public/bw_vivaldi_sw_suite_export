# Docs set at Bluewind for Vivaldi app documentation

<img src="assets/bwlogo.jpg" width="400"/>

This repository contains useful documentation and code of the Vivaldi Sound Recognition Platform developed at Bluewind.  
For additional information please refer to __Contacts__.

### Language

Training language is English (for written matter).

# The Vivaldi Platform

The Vivaldi Sound Recognition Platform is an embedded software suite targeting Low Power MCU of the STM32 series.  
The target hardware is the [Sensortile Developement Kit](https://www.st.com/en/evaluation-tools/steval-stlkt01v1.html).  

The Vivaldi Platform enables run-time Sound Event Classification and it targets several sectors:  
* anomaly detection in safety systems
* key work spotting in robotics
* predictive maintenance for indutrial machinery and mechanical systems
* sound recognition in automation
* intelligent transportation systems
* smart cities

In this embodiment the Vivaldi Suite is deployed for real time traffic monitoring.

More information regarding the overall working principle of the application, the software architecture, the development process and more
please read the Vivaldi project [white paper](docs/VivaldiProject_overview_Rev5.pdf).

# Development Goal

## Requirements

The following items are required:  

* Sensortile kit (see link above)
* [NUCLEO F401 board](https://www.st.com/en/evaluation-tools/nucleo-f401re.html)
* [Personal account on Gitlab](https://gitlab.com/users/sign_in?redirect_to_referer=yes&nav_source=navbar)
* [Atollic Truestudio](https://atollic.com/resources/download/)

## Goal

Substitute the audio pre-processing algorithm with ADA functions.

### Contacts

- 2019 Stefano Costa, Bluewind
- 2019 Pietro Montino, Bluewind
